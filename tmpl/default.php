<!-- Rokoho Form module -->

<div class="contact-form ">
  <form method="post" id="emailform" action="<?php echo $doc->baseurl ?>" role="form" >
    <div class="form-group">
      <label for="fname"><?php echo JText::_('LABEL_NAME'); ?></label>
      <input 
            type="text" 
            name="fname" 
            id="fname" 
            data-validation="required"
            class="form-control" 
            placeholder="<?php echo JText::_('PLACEHOLDER_NAME'); ?>" />
    </div>
    <div class="form-group">
      <label for="femail"><?php echo JText::_('LABEL_EMAIL'); ?></label>
      <input 
            type="text" 
            name="femail" 
            id="femail" 
            data-validation="email"
            class="required validate-email form-control" 
            placeholder="<?php echo JText::_('PLACEHOLDER_EMAIL'); ?>" />
    </div>
    <div class="form-group">
      <label for="fsubject"><?php echo JText::_('LABEL_SUBJECT'); ?></label>
      <input 
            type="text" 
            name="fsubject" 
            id="fsubject" 
            data-validation="required"
            class="required form-control" 
            placeholder="<?php echo JText::_('PLACEHOLDER_SUBJECT'); ?>" />
    </div>
    <div class="form-group">
      <label for="fmessage"><?php echo JText::_('LABEL_MESSAGE'); ?></label>
      <textarea 
            name="fmessage" 
            id="fmessage" 
            data-validation="required"
            class="required form-control" 
            placeholder="<?php echo JText::_('PLACEHOLDER_MESSAGE'); ?>" ></textarea>
    </div>
    <button 
        name="submit" 
        id="submit" 
        type="submit" 
        value="Send Email" 
        class="btn btn-danger"> <?php echo JText::_('BUTTON_SEND'); ?> </button>
    <?php echo JHtml::_('form.token'); ?>
  </form>
  <script>
		jQuery(document).ready(function($) {
			 $.validate(); 
		});
	</script> 
</div>

<!-- End Contact Form Module -->