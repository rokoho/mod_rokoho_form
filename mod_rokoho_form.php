<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
// Include the syndicate functions only once
require_once( dirname(__FILE__).'/helper.php' );

require( JModuleHelper::getLayoutPath( 'mod_rokoho_form' ) );

$cid = $params->get('cid');

$contact_details = modFormHelper::getContactEmail($cid);

if($_POST['submit']!='' )
{
	if(JSession::checkToken())
	{
		modFormHelper::sendmail($_POST['femail'],strip_tags($_POST['fname']),$contact_details['email_to'], '[' . $params->get('csubprefix') . '] ' . strip_tags($_POST['fsubject']),strip_tags($_POST['fmessage']));
	}
	else
	{
		die( 'Invalid Token' );
	}
}
?>